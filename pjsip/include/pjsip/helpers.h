#pragma once

#include <stdio.h>
#include <pjsip/sip_msg.h>

static inline
void print_message(const char* msg_header, const pjsip_msg* msg)
{
	char buffer[1024];
    pjsip_msg_print(msg, buffer, 1024);
	printf("[%s] %s\n", msg_header, buffer);
}
